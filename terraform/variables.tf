variable "ssh_key_public" {
  type        = string
  default     = "your_public_key"
}

variable "key_name" {
  description = "Name of the key used to access the server"
  type        = string
  default     = "deployer-key"
}

variable "management_range" {
  type        = string
  default     = "0.0.0.0/0"
}

variable "vpc_cidr" {
    description = "CIDR block of the VCN"
    default     = "10.0.0.0/16"
    type        = string
}

variable "server_port" {
  description = "The port the server will use for HTTP requests"
  type        = number
  default     = 8080
}

variable "alb_name" {
  description = "The name of the ALB"
  type        = string
  default     = "app1-lb"
}

variable "instance_security_group_name" {
  description = "The name of the security group for the EC2 Instances"
  type        = string
  default     = "app1-sg"
}

variable "alb_security_group_name" {
  description = "The name of the security group for the ALB"
  type        = string
  default     = "app1-lb-sg"
}



