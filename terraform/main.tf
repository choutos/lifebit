provider "aws" {
  region = "eu-central-1"
}

resource "aws_key_pair" "deployer" {
  key_name   = "deployer-key"
  public_key = var.ssh_key_public
}

resource "aws_vpc" "lifebit-vpc" {
  cidr_block = var.vpc_cidr

  tags = {
    Name = "lifebit"
  }
}

resource "aws_subnet" "subnet-1" {
  vpc_id = aws_vpc.lifebit-vpc.id
  cidr_block = "10.0.1.0/24"
  map_public_ip_on_launch = true

  availability_zone = "eu-central-1a"

  tags = {
    Name = "app1-subnet1"
  }

}

resource "aws_subnet" "subnet-2" {
  vpc_id = aws_vpc.lifebit-vpc.id
  cidr_block = "10.0.2.0/24"
  map_public_ip_on_launch = true

  availability_zone = "eu-central-1c"

  tags = {
    Name = "app1-subnet2"
  }

}

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.lifebit-vpc.id
}

resource "aws_route_table" "inet-route-table" {
  vpc_id = aws_vpc.lifebit-vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

    route {
    ipv6_cidr_block        = "::/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = {
    Name = "inet"
  }
}

resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.subnet-1.id
  route_table_id = aws_route_table.inet-route-table.id
}

resource "aws_route_table_association" "b" {
  subnet_id      = aws_subnet.subnet-2.id
  route_table_id = aws_route_table.inet-route-table.id
}

resource "aws_launch_configuration" "app1" {
  image_id        = "ami-0502e817a62226e03"
  instance_type   = "t2.micro"
  security_groups = [aws_security_group.instance-sg.id]
  key_name = var.key_name

    user_data = "${file("user-data.sh")}"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "app1" {
  launch_configuration = aws_launch_configuration.app1.name
  vpc_zone_identifier  = [aws_subnet.subnet-1.id,aws_subnet.subnet-2.id]

  target_group_arns = [aws_lb_target_group.asg.arn]
  health_check_type = "ELB"

  min_size = 2
  max_size = 10

  tag {
    key                 = "Name"
    value               = "app1-asg"
    propagate_at_launch = true
  }
}

resource "aws_autoscaling_policy" "cpu" {
  name        = "Target Tracking Policy"
  policy_type = "TargetTrackingScaling"
  autoscaling_group_name = aws_autoscaling_group.app1.name
  target_tracking_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ASGAverageCPUUtilization"
    }
    target_value = 5  # We set a low value to test the autoscaling
  }
}

resource "aws_security_group" "instance-sg" {
  name = var.instance_security_group_name
  vpc_id   = aws_vpc.lifebit-vpc.id

  ingress {
    from_port   = var.server_port
    to_port     = var.server_port
    protocol    = "tcp"
    cidr_blocks = [var.management_range]
  }
    ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.management_range]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}


resource "aws_lb" "app1-lb" {

  name               = var.alb_name

  load_balancer_type = "application"
  subnets            = [aws_subnet.subnet-1.id,aws_subnet.subnet-2.id]
  security_groups    = [aws_security_group.alb.id]
}

resource "aws_lb_listener" "http" {
  load_balancer_arn = aws_lb.app1-lb.arn
  port              = 80
  protocol          = "HTTP"

  # By default, return a simple 404 page
  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "404: page not found"
      status_code  = 404
    }
  }
}

resource "aws_lb_target_group" "asg" {

  name = var.alb_name

  port     = var.server_port
  protocol = "HTTP"
  vpc_id   = aws_vpc.lifebit-vpc.id

  health_check {
    path                = "/"
    protocol            = "HTTP"
    matcher             = "200"
    interval            = 15
    timeout             = 10
    healthy_threshold   = 2
    unhealthy_threshold = 5
  }
}

resource "aws_lb_listener_rule" "asg" {
  listener_arn = aws_lb_listener.http.arn
  priority     = 100

  condition {
    path_pattern {
      values = ["*"]
    }
  }

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.asg.arn
  }
}

resource "aws_security_group" "alb" {

  name = var.alb_security_group_name
  vpc_id      = aws_vpc.lifebit-vpc.id

  # Allow inbound HTTP requests
  ingress {
    description = "HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Allow all outbound requests
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

