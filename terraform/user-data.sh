#!/bin/bash

##### Install Ansible ######
sudo apt update -y
sudo apt install git ansible -y


mkdir -p /tmp/ansible
git clone https://gitlab.com/choutos/lifebit-ansible.git /tmp/ansible
cd /tmp/ansible
ansible-playbook deploy.yml --connection=local -i localhost

