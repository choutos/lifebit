## Overview

### Provisioning
Simple architecture to provide high availability and autoscaling capabilities for a "Hello World" application.
The provisioning of the infrastructure is done with Terraform, which creates:
- A new VPC
- Two subnets in two different availability domains
- A Load Balancer
- An autoscaling group
- Internet Gateway to provide internet access from the instances

And all the security rules and policies necessary to make the whole work.

![architecture](architecture.png "Architecture")

### Deployment
The deployment part is done automatically when a new instance is created. For that I use a different repo: https://gitlab.com/choutos/lifebit-ansible.git.

Via user_data (see ![user-data.sh](terraform/user-data.sh)) this repo is cloned, and an ansible playbook takes care of installing and starting the application.


### Security
The management access can be limited setting the variable **management_range** in ![variables.tf](terraform/variables.sh) to the required public range.

### Load test
In order to test the autoscaling, I decided to use a simple ![script](scripts/loadtest.sh) to generate calls to the public endpoint.
It can be improved using JMeter in multiple compute instances or complete third party solutions.

Usage:
`scripts/loadtest.sh <number_of_call_per_second> <public_endpoint>`

##  Deployment Instructions

###  Requirements
Terraform installed and configured to work with your AWS account

### Steps

Clone this repo
```
git clone https://gitlab.com/choutos/lifebit.git
cd lifebit
```

Edit terraform/variables.tf and add your public key in:

```
variable "ssh_key_public" {
  type        = string
  default     = "your_public_key"
}
```

Trigger provisioning and deployment:
```
terraform apply
```

